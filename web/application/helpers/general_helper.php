<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function load_image($file='')
{
    if (empty($file)) {
        return "data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==";
    }else{
        $filename = './'.$file;

        if (is_file($filename)  && file_exists($filename) ) {
            return base_url($file);
        } else {
            return base_url('assets/global/img/noimage.jpg');
        }
    }
}

function load_thumb($file='')
{
    if (empty($file)) {
        return "data:image/gif;base64,R0lGODlhAQABAIAAAMLCwgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==";
    }else{
        $file = get_thumb($file);

        $filename = './'.$file;

        if (is_file($filename)  && file_exists($filename) ) {
            return base_url($file);
        } else {
            return base_url('assets/global/img/noimage.jpg');
        }
    }
}

function get_thumb($file='')
{
    $extension_pos = strrpos($file, '.'); // find position of the last dot, so where the extension starts
    $file = substr($file, 0, $extension_pos) . '_thumb' . substr($file, $extension_pos);
    return $file;
}

function delete_image($file)
{
    //delete main file
    if (is_file($file)  && file_exists($file) ) {
        unlink($file);
    }
    //delete thumb file
    $extension_pos = strrpos($file, '.'); // find position of the last dot, so where the extension starts
    $file = substr($file, 0, $extension_pos) . '_thumb' . substr($file, $extension_pos);
    if (is_file($file)  && file_exists($file) ) {
        unlink($file);
    }
}

function terbilang($x){
  $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  if ($x < 12)
    return " " . $abil[$x];
  elseif ($x < 20)
    return Terbilang($x - 10) . " Belas";
  elseif ($x < 100)
    return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
  elseif ($x < 200)
    return " Seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
    return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
    return " Seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
    return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
    return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
    elseif ($x < 1000000000000)
    return Terbilang($x / 1000000000) . " Milyar" . Terbilang($x % 1000000000);
}
