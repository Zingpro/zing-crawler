<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('cek_hak_akses'))
{
    function cek_hak_akses($usergroup_id = '', $menu_kode='')
    {
        $ci =& get_instance();
        $sql = "
        SELECT 
            akses_id 
        FROM t_akses
        LEFT JOIN m_menu ON t_akses.menu_id = m_menu.menu_id

        where
            usergroup_id = $usergroup_id and
            menu_kode = '$menu_kode' and
            akses_active = 'y'
        ";

        if($ci->db->query($sql)->num_rows() <= 0){
            // redirect(site_url('404_override'));
        }
    }
}

if ( ! function_exists('cek_hak_akses_formx'))
{
    function cek_hak_akses_formx($usergroup_id = '', $menu_kode='')
    {
        $ci =& get_instance();
        $ci->load->library('session');
        if (!empty($ci->input->post('flow_id'))) {
            $menu_kode = 'flow_'.$ci->input->post('flow_id');
        }
        elseif (!empty($ci->input->post('parent_form_id'))) {
            $menu_kode = 'form_'.$ci->input->post('parent_form_id');
        }
        cek_hak_akses($usergroup_id,$menu_kode);
    }
}

if ( ! function_exists('get_listakses'))
{
    function get_listakses($usergroup_id = '', $menu_kode='')
    {
        $ci =& get_instance();
        $ar_haklistakses = [];

        $sql = "
        SELECT akses_code 
        FROM t_akses
        LEFT JOIN m_menu ON t_akses.menu_id = m_menu.menu_id
        where
            usergroup_id = $usergroup_id and
            menu_kode = '$menu_kode' and
            akses_active = 'y'
        ";
        $q = $ci->db->query($sql);
        if ($q->num_rows() > 0) {
            $akses_code = $q->row()->akses_code;
            if(!$ar_haklistakses = json_decode($akses_code,true)){
                $ar_haklistakses = [];
            }
        }

        return $ar_haklistakses;
    }
}

if ( ! function_exists('get_listakses_formx'))
{
    function get_listakses_formx($usergroup_id = '', $menu_kode='')
    {
        $ci =& get_instance();
        $ci->load->library('session');
        if ($ci->input->post('authorize') == 'readonly') {
            return $ar_haklistakses = array('xread');
        }
        if (!empty($ci->input->post('flow_id'))) {
            $menu_kode = 'flow_'.$ci->input->post('flow_id');
            return $ar_haklistakses = array('xcreate','xupdate','xdelete');
        }
        // if (!empty($ci->input->post('parent_form_id'))) {
        //     $menu_kode = 'form_'.$ci->input->post('parent_form_id');
        // }
        return get_listakses($usergroup_id,$menu_kode);
    }
}

if ( ! function_exists('tree_menu'))
{
    function tree_menu($usergroup_id = '',$menu_kode='z')
    {
        $ci =& get_instance();
        // $usergroup_id = $this->session->userdata('usergroup_id');
        $sql = "
            SELECT *
            FROM t_akses
            LEFT JOIN m_menu ON t_akses.menu_id = m_menu.menu_id
            WHERE usergroup_id = $usergroup_id
                    and akses_active = 'y'
                    and menu_active = 'y'
            ORDER BY menu_parent, menu_order asc;
        ";
        $items = $ci->db->query($sql)->result_array();
        if (count($items) == 0) {
            return '';
        }

        $html = '';
        $parent = 0;
        $parent_stack = array();

        // $items contains the results of the SQL query
        $children = array();
        foreach ( $items as $item )
            $children[$item['menu_parent']][] = $item;

        while ( ( $option = each( $children[$parent] ) ) || ( $parent > 0 ) )
        {
            if ( !empty( $option ) )
            {
                if ($option['value']['menu_kode'] == $menu_kode)
                    $selected = 'active open';
                else
                    $selected = '';
                // 1) The item contains children:
                // store current parent in the stack, and update current parent
                if ( !empty( $children[$option['value']['menu_id']] ) )
                {
                    $icon = (isset($option['value']['menu_icon'])) ? 'fa fa-'.$option['value']['menu_icon'] : 'fa fa-angle-right' ;
                    $html .= '<li class="nav-item start '.$selected.'">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="'.$icon.'"></i>
                                    <span class="title"><b>' . $option['value']['menu_nama'] . '</b></span>
                                </a>
                                ';
                    $html .= '<ul class="sub-menu">';
                    array_push( $parent_stack, $parent );
                    $parent = $option['value']['menu_id'];
                }
                // 2) The item does not contain children
                else{
                    $icon = (isset($option['value']['menu_icon'])) ? 'fa fa-'.$option['value']['menu_icon'] : 'fa fa-gear' ;
                    $html .= '<li class="nav-item start '.$selected.'">
                                <a href="'.site_url($option['value']['menu_url']).'" class="nav-link nav-toggle">
                                    <i class="'.$icon.'"></i>
                                    <span class="title">' . $option['value']['menu_nama'] . '</span>
                                </a>
                                </li>';
                }
            }
            // 3) Current parent has no more children:
            // jump back to the previous menu level
            else
            {
                $html .= '</ul></li>';
                $parent = array_pop( $parent_stack );
            }
        }

        // At this point, the HTML is already built
        return $html;
    }
}
