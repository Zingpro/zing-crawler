<!-- Main content -->
<section class='content'>
  <div class='row'>
    <div class='col-md-12'>
      <div class='portlet light'>
        <div class='portlet-title'>
          <div class='caption font-green'>
            <span class='caption-subject bold uppercase'>Change password </span>
          </div>
        </div>
        <div class='portlet-body form'>
          <form action="<?php echo $action; ?>" method="post" id="input_form" class="" role="form" >
            <div class='form-body'>
              <div class='form-group'>
                <label class='control-label'>Old password</label>
                <div class='input-group'>
                  <span class="input-group-addon">
                      <i class="fa fa-key"></i>
                  </span>
                  <input type="password" class="form-control" name="old_password" placeholder="Old password"  />
                </div>
              </div>
              <div class="form-group">
                  <label>New password</label>
                  <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-key font-green"></i>
                    </span>
                  <input type="password" class="form-control" name="password" placeholder="New password"  />
                  </div>
              </div>
              <div class="form-group">
                  <label>Confirm password</label>
                  <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-key font-green"></i>
                    </span>
                  <input type="password" class="form-control" name="rpassword" placeholder="Confirm password"  />
                  </div>
              </div>
            </div>
            <div class='form-actions right1'>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type='submit' class='btn green' name='mode' value='new' >Simpan</button>
            </div>
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
  $('#input_form').submit(function(e) {
        e.preventDefault();
        main.submitAjaxModal($(this));
  });
</script>
