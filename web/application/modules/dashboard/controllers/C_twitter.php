<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class C_twitter extends MY_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('M_twitter');
        $this->load->model('formx/Formx_model');
        $this->load->library('form_validation');
    }
    

    public function index()
    {
        $this->template('v_twitter_list');
    }

    public function getDatatable()
    {
        $customActionName=$this->input->post('customActionName');
        $records         = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = ($this->input->get_post('length')) ? $this->input->get_post('length') : 10 ;
        $iDisplayStart  = ($this->input->get_post('start')) ? $this->input->get_post('start') : 0 ;
        $sEcho          = ($this->input->get_post('draw')) ? $this->input->get_post('draw') : 1 ;

        $t              = $this->M_twitter->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="group-checkable" name="id[]" value="'.$d->twitter_id.'"><span></span></label>';
                $view  = '<button class="btn btn-icon-only green openmodal" title="view"  data-url="'.site_url('dashboard/c_twitter/read/'.$d->twitter_id).'"><i class="fa fa-search fa-lg"></i></button>';
                $edit  = '<button class="btn btn-icon-only blue openmodal" title="edit"  data-url="'.site_url('dashboard/c_twitter/form/'.$d->twitter_id).'"><i class="fa fa-pencil-square-o fa-lg"></i></button>';
                $delete  = '<button class="btn btn-icon-only red delete" title="delete" data-title="'.$d->{$this->M_twitter->label}.'" data-url="'.site_url('dashboard/c_twitter/delete/'.$d->twitter_id).'"><i class="fa fa-trash fa-lg"></i></button>';
                $add_btn = '';

                $row = array();
                // $row[] = $checkbok;
                
				$row[] =$d->twitter_post; 

                // $row[] = $add_btn.$view.$edit.$delete;
                $records["data"][] = $row;
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function read($id)
    {
        $this->data['authorize'] = 'readonly';
        $this->form($id);
    }

    public function form($id=null)
    {
        $data['action']    = site_url('dashboard/c_twitter/form_action');
        if (empty($id)) {
            $data['row']       = null;
            $data['id']        = '';
        }else{
            $row = $this->M_twitter->get($id);

            if ($row) {
                $data['row']       = $row;
                $data['id']        = $row->twitter_id;
            } else {
                show_error('Data not found');
            }
        }

        $this->template('v_twitter_form',$data);
    }
    
    public function form_action()
    {
        $res['success'] = false;
        $res['message'] = 'Simpan gagal';
        
        $this->form_validation->set_rules('twitter_post','twitter_post','trim');

        if ($this->form_validation->run() == FALSE) {
            $res['message'] = 'Lengkapi form dengan benar';
            $res['field_error'] = $this->form_validation->error_array();
        } else {
            $data = array(
                        'twitter_post' => $this->input->post('twitter_post'),
                    );
            if (empty($this->input->post('twitter_id'))) {
                if($this->M_twitter->insert($data)){
                    $res['datatable']     = 'datatableC_twitter';
                    $res['success'] = true;
                    $res['message'] = 'Simpan berhasil';
                }
            }else{
                $row = $this->M_twitter->get($this->input->post('twitter_id'));

                if ($row) {
                    if($this->M_twitter->update($data,$this->input->post('twitter_id'))){
                        $res['datatable']     = 'datatableC_twitter';
                        $res['success'] = true;
                        $res['message'] = 'Simpan berhasil';
                    }
                } else {
                    $res['success'] = false;
                    $res['message'] = 'Data not found';
                }
            }
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    
    public function delete($id)
    {
        $res['success'] = false;
        $res['message'] = 'Hapus gagal';
        $row = $this->M_twitter->get($id);

        if ($row) {
            $this->M_twitter->delete($id);
            $res['success'] = true;
            $res['message'] = 'Hapus berhasil';
        } else {
            $res['message'] = 'Data tidak ditemukan';
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function delete_checked()
    {
        $id_array=$this->input->post('id[]');
        foreach ($id_array as $id) {
            $row = $this->M_twitter->get($id);

            if ($row) {
                $this->M_twitter->delete($id);
            }
        }
        $result["customActionStatus"]="OK";
        $result["customActionMessage"]="Delete Record Success";
        return $result;
    }
}
    
