<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function index()
    {
        $data = array( );
        $data['popular_keyword'] = $this->popular_keyword();
        $data['popular_location'] = $this->popular_location();
        $data['jumlah_tweet_all'] = $this->count_tweet();
        $data['jumlah_tweet_this_month'] = $this->count_tweet_this_month();
        $data['jumlah_tweet_this_week'] = $this->count_tweet_this_week();

        $this->template('v_dashboard2', $data, FALSE);
    }

    public function count_tweet()
    {
        $sql= " SELECT COUNT(twitter_id)  as jumlah from twitter";
        return $this->db->query($sql)->row()->jumlah;
    }

    public function count_tweet_this_month()
    {
        $sql= " SELECT COUNT(twitter_id)  as jumlah from twitter 
                WHERE MONTH(twitter_date_crawl_created) = MONTH(CURRENT_DATE())
                    AND YEAR(twitter_date_crawl_created) = YEAR(CURRENT_DATE());
        ";
        return $this->db->query($sql)->row()->jumlah;
    }
    public function count_tweet_this_week()
    {
        $sql= " SELECT COUNT(twitter_id)  as jumlah from twitter 
                WHERE YEARWEEK(`twitter_date_crawl_created`, 1) = YEARWEEK(CURDATE(), 1)
        ";
        return $this->db->query($sql)->row()->jumlah;
    }

    public function popular_location()
    {
        $sql=" 
            SELECT twitter_user_location, count(twitter_id) as jumlah 
            from twitter  
            where twitter_user_location != ''
            group BY twitter_user_location 
            order by jumlah 
            desc LIMIT 10";
        return $this->db->query($sql)->result();
    }

    public function popular_keyword()
    {
		$sql=" 
    		SELECT twitter_keyword, count(twitter_id) as jumlah 
    		from twitter  
    		group BY twitter_keyword 
    		order by jumlah 
    		desc LIMIT 10";
        return $this->db->query($sql)->result();
    }
	
	public function load_tp2(){
		$url = "https://twitter.com/rtmcjatim?lang=en";
		
		$context = stream_context_create(array('http' => array('header' => 'User-Agent: Mozilla compatible')));
		$response = file_get_contents($url, false, $context);
		$html = str_get_html($response);
		echo $html;
	}
	
	public function load_trending_topic()
	{
		$url = "https://twitter.com/spydzing";
		$data = array( );
		
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$output = curl_exec($ch);
		
		if ($output === FALSE) { 
		  $data['curl_error_data'] = "cURL Error: " . curl_error($ch);		 
		}
		$data['url_data'] = curl_getinfo($ch); 
		$data['url_output'] = $output;
		curl_close($ch);
		//$data['url_content'] = file_get_contents($url);
		
		$this->load->library('dom_parser');
		
		
		$html = $this->dom_parser->file_get_html($url);
		//page-container > div.AppContainer > div > div > div.Grid-cell.u-size2of3.u-lg-size3of4 > div > div.Grid-cell.u-size1of3 > div > div > div > div > div > div.module.Trends.trends
		foreach($html->find('div[class=module]') as $a) {
			echo $a->innertext;
		}
		$html->clear();
		unset($html);
		//echo $html;
				
        $this->load->view('v_wrap_trending_topic', $data, false);
	}

}

/* End of file Dashboard.php */
/* Location: ./application/modules/dashboard/controllers/Dashboard.php */
