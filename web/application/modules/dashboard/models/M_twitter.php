<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_twitter extends MY_Model
{

    public $table = 'twitter';
    public $primary_key = 'twitter_id';
    public $label = 'twitter_id';
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array('twitter_id'); // ...Or you can set an array with the fields that cannot be filled by insert/update

    function __construct()
    {
        parent::__construct();
            $this->soft_deletes = FALSE;
            $this->timestamps = FALSE;
        
    }

    // get total rows
    function get_limit_data($limit, $start) {
        $order            = $this->input->post('order');
        $dataorder = array();
        $where = array();

        $i=0;
        $dataorder[$i++] = 'twitter_date_crawl_created';
        if(!empty($this->input->post('twitter_post'))){
            $where['LOWER(twitter_post) LIKE'] = '%'.strtolower($this->input->post('twitter_post')).'%';
        }

        $this->where($where);
        $result['total_rows'] = $this->count_rows();

        $this->where($where);
        if ($order) {
            $this->order_by( $dataorder[$order[0]["column"]],  $order[0]["dir"]);
        }
        $this->limit($start, $limit);
        $result['get_db']=$this->get_all();
        return $result;
    }

}

