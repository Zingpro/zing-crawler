<div class="row">
  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="dashboard-stat2 ">
          <div class="display">
              <div class="number">
                  <h3 class="font-green-sharp">
                      <span data-counter="counterup" data-value="<?php echo $jumlah_tweet_all ?>"><?php echo $jumlah_tweet_all ?></span>
                      <small class="font-green-sharp"></small>
                  </h3>
                  <small>Total Tweet</small>
              </div>
              <div class="icon">
                  <i class="icon-pie-chart"></i>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="dashboard-stat2 ">
          <div class="display">
              <div class="number">
                  <h3 class="font-red-haze">
                      <span data-counter="counterup" data-value="<?php echo $jumlah_tweet_this_month ?>"><?php echo $jumlah_tweet_this_month ?></span>
                      <small class="font-red-haze"></small>
                  </h3>
                  <small>Total Tweet This Month</small>
              </div>
              <div class="icon">
                  <i class="icon-pie-chart"></i>
              </div>
          </div>
      </div>
  </div>
  <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
      <div class="dashboard-stat2 ">
          <div class="display">
              <div class="number">
                  <h3 class="font-purple-soft">
                      <span data-counter="counterup" data-value="<?php echo $jumlah_tweet_this_week ?>"><?php echo $jumlah_tweet_this_week ?></span>
                      <small class="font-purple-soft"></small>
                  </h3>
                  <small>Total Tweet This Week</small>
              </div>
              <div class="icon">
                  <i class="icon-pie-chart"></i>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="portlet light">
          <div class='portlet-title'>
              <div class="caption">
                  <i class="icon-settings font-dark"></i>
                  <span class="caption-subject font-dark sbold uppercase">Tweet</span>
              </div>                            
          </div><!-- /.box-header -->
          <div class='portlet-body'>
            <div class='table-container'>
                <table class="table table-striped table-bordered table-hover" id="datatableC_twitter">
                    <thead>
                        <tr role="row" class="heading">
                            <th>twitter post</th>
                        </tr>
                        <tr role="row" class="filter">
                            <td><input type="text" class="form-control form-filter input-sm" name="twitter_post"></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.table-container -->
          </div>
        </div>
    </div>
    <div class="col-md-4">
		  <div class="portlet light">
          <div class='portlet-title'>
              <div class="caption">
                  <i class="icon-settings font-dark"></i>
                  <span class="caption-subject font-dark sbold uppercase">Diagram Keyword</span>
              </div>                            
          </div><!-- /.box-header -->
          <div class='portlet-body'>
    				<div id="chart_5" class="chart" style="height: 400px;">
    				</div>
          </div>
      </div>
		
		  <div class="portlet light">
          <div class='portlet-title'>
              <div class="caption">
                  <i class="icon-settings font-dark"></i>
                  <span class="caption-subject font-dark sbold uppercase">Popular keyword</span>
              </div>                            
          </div><!-- /.box-header -->
          <div class='portlet-body'>
            <table class="table table-bordered">
              <tr>
                <th>Keyword</th>
                <th>Jumlah</th>
              </tr>
              <?php foreach ($popular_keyword as $r): ?>
                 <tr>
                   <td><?php echo $r->twitter_keyword ?></td>
                   <td><span class="badge badge-success"> <?php echo $r->jumlah ?> </span></td>
                 </tr>
              <?php endforeach ?>
            </table>
          </div>
      </div>
    </div>
    <div class="col-md-4">
		  <div class="portlet light">
          <div class='portlet-title'>
              <div class="caption">
                  <i class="icon-settings font-dark"></i>
                  <span class="caption-subject font-dark sbold uppercase">Diagram Keyword</span>
              </div>                            
          </div><!-- /.box-header -->
          <div class='portlet-body'>
				<div id="chart_7" class="chart" style="height: 400px;">
				</div>
          </div>
      </div>
      <div class="portlet light">
          <div class='portlet-title'>
              <div class="caption">
                  <i class="icon-settings font-dark"></i>
                  <span class="caption-subject font-dark sbold uppercase">Location</span>
              </div>                            
          </div><!-- /.box-header -->
          <div class='portlet-body'>
            <table class="table table-bordered">
              <tr>
                <th>Location</th>
                <th>Jumlah</th>
              </tr>
              <?php foreach ($popular_location as $r): ?>
                 <tr>
                   <td><?php echo $r->twitter_user_location ?></td>
                   <td><span class="badge badge-success"> <?php echo $r->jumlah ?> </span></td>
                 </tr>
              <?php endforeach ?>
            </table>
          </div>
      </div>
    </div>
</div>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/amcharts.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/serial.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/pie.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/radar.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/light.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/patterns.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amcharts/themes/chalk.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/ammap.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/global/plugins/amcharts/amstockcharts/amstock.js') ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script>
  var datatableC_twitter = new Datatable();
    datatableC_twitter.init({
        src: $("#datatableC_twitter"),
        dataTable: {
          "dom": "ptp", // datatable layout
            "ajax": {
                "url": "<?php echo site_url('dashboard/c_twitter/getDatatable/') ?>", // ajax source
            },
            "order": [
                [0, "desc"]
            ],// set first column as a default sort by asc
        }
    });
  
  var initChartSample5 = function() {
        var chart = AmCharts.makeChart("chart_5", {
            "theme": "light",
            "type": "serial",
            "startDuration": 2,
            "fontFamily": 'Open Sans',            
            "color":    '#888',
            "dataProvider": [
			<?php 
				$c = 0;
				$color = array('#FF0F00','#eea638','#a7a737','#d8854f','#de4c4f');
				foreach ($popular_keyword as $r): 
					$c++;
					if($c <= 5){ 
						
			?>
			{
                "country": "<?php echo $r->twitter_keyword ?>",
                "visits": <?php echo $r->jumlah ?>,
                "color": "<?php echo $color[$c-1] ?>"
            },
			<?php 
					}
			endforeach; 
			?>
			],
            "valueAxes": [{
                "position": "left",
                "axisAlpha": 0,
                "gridAlpha": 0
            }],
            "graphs": [{
                "balloonText": "[[category]]: <b>[[value]]</b>",
                "colorField": "color",
                "fillAlphas": 0.85,
                "lineAlpha": 0.1,
                "type": "column",
                "topRadius": 1,
                "valueField": "visits"
            }],
            "depth3D": 40,
            "angle": 30,
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0

            },
            "exportConfig": {
                "menuTop": "20px",
                "menuRight": "20px",
                "menuItems": [{
                    "icon": '/lib/3/images/export.png',
                    "format": 'png'
                }]
            }
        }, 0);

        
        $('#chart_5').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
	initChartSample5();
	
	var initChartSample7 = function() {
        var chart = AmCharts.makeChart("chart_7", {
            "type": "pie",
            "theme": "light",
            "fontFamily": 'Open Sans',
            "color":    '#888',
            "dataProvider": [
			<?php 
				$c = 0;
				$color = array('#FF0F00','#eea638','#a7a737','#d8854f','#de4c4f');
				foreach ($popular_keyword as $r): 
					$c++;
					if($c <= 5){ 
						
			?>
			{
                "country": "<?php echo $r->twitter_keyword ?>",
                "value": <?php echo $r->jumlah ?>
            },
			<?php 
					}
			endforeach; 
			?>
			],
            "valueField": "value",
            "titleField": "country",
            "outlineAlpha": 0.4,
            "depth3D": 15,
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "angle": 30,
            "exportConfig": {
                menuItems: [{
                    icon: '/lib/3/images/export.png',
                    format: 'png'
                }]
            }
        });
        
        $('#chart_7').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
    }
	initChartSample7();
</script>
