
<!-- Main content -->
<div class='page-content'>
  <div class='row'>
    <div class='col-md-12'>
      <div class='portlet box green'>
        <div class='portlet-title'>
          <div class='caption'>
            <span class='caption-subject bold uppercase'>Form C_TWITTER </span>
          </div>
        </div>
        <div class='portlet-body form'>
          <form action="<?php echo $action; ?>" method="post" id="input_form_c_twitter" class="horizontal-form <?php echo $authorize ?>"  >
            <div class='form-body'>
              <div class='row'>
                <div class=''col-md-6''>
                  <div class='form-group'>
                    <label class='control-label'>twitter post</label>
                    <div class=''>
                      <textarea class="form-control <?php echo 'data-'.$authorize ?>" rows="3" name="twitter_post" id="twitter_post" placeholder="twitter post"><?php echo (isset($row)) ? $row->twitter_post : ''; ?></textarea>
                    </div>
                  </div>
                </div>
                
                <input type="hidden" name="twitter_id" value="<?php echo $id; ?>" />
              </div>
            </div>
            <div class='form-actions right'>
              <div class='row'>
                <div class='col-md-offset-5 col-md-7'>
                  <?php if (!$modal): ?>
                  <a href="<?php echo site_url('dashboard/c_twitter') ?>" class="btn default">Kembali</a>
                  <?php else: ?>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  <?php endif ?>
                  <?php if ($authorize == 'edit'): ?>
                  <button type='submit' class='btn green' >Simpan</button>
                  <?php endif ?>
                </div>
              </div>
            </div>
              
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.content -->

<script type="text/javascript">
  $('#input_form_c_twitter').submit(function(e) {
        e.preventDefault();
        main.submitAjaxModal($(this));
  });
</script>

