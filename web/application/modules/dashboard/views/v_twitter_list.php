
<!-- Main content -->
<div class=''>
  <div class='row'>
    <div class='col-md-12'>
      <div class='portlet light portlet-fit portlet-datatable bordered'>
        <div class='portlet-title'>
            <div class="caption">
                <span class="caption-subject font-green sbold uppercase">C_TWITTER  </span>
            </div>
            <div class="actions">
                <div class="btn-group" >
                    <button data-url="<?php echo site_url('dashboard/c_twitter/form/')?>" class="btn btn-circle btn-info btn-sm openmodal">
                        <i class="fa fa-pencil"></i> Create
                    </button>
                </div>
                <div class="btn-group">
                    <a class="btn red btn-circle" href="javascript:;" data-toggle="dropdown">
                        <i class="fa fa-share"></i>
                        <span class="hidden-xs"> Tools </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu pull-right" id="datatable_ajax_tools">
                        <li>
                            <a href="javascript:;" data-action="0" class="tool-action">
                                <i class="icon-printer"></i> Print</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-action="1" class="tool-action">
                                <i class="icon-check"></i> Copy</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-action="2" class="tool-action">
                                <i class="icon-doc"></i> PDF</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-action="3" class="tool-action">
                                <i class="icon-paper-clip"></i> Excel</a>
                        </li>
                        <li>
                            <a href="javascript:;" data-action="4" class="tool-action">
                                <i class="icon-cloud-upload"></i> CSV</a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="javascript:;" data-action="5" class="tool-action">
                                <i class="icon-refresh"></i> Reload</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- /.box-header -->
        <div class='portlet-body'>
            <div class='table-container'>
                <div class="table-actions-wrapper">
                    <span> </span>
                    <select class="table-group-action-input form-control input-inline input-small input-sm">
                        <option value="">Select...</option>
                        <option value="delete">Delete</option>
                    </select>
                    <button class="btn btn-sm green table-group-action-submit">
                        <i class="fa fa-check"></i> Submit</button>
                </div>
                <table class="table table-striped table-bordered table-hover" id="datatableC_twitter">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="2%" class="not-export-column">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" />
                                    <span></span>
                                </label>
                            </th>
                    
                            <th>twitter post</th>
                            <th width="2%" class="not-export-column">Action</th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                    
                            <td><input type="text" class="form-control form-filter input-sm" name="twitter_post"></td>
                            <td>
                                <div class="margin-bottom-5">
                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                    <i class="fa fa-search"></i> Search</button>
                                </div>
                                <button class="btn btn-sm red btn-outline filter-cancel">
                                <i class="fa fa-times"></i> Reset</button>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div><!-- /.table-container -->
        </div><!-- /.portlet-body -->
      </div><!-- /.portlet -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.content -->

<script type="text/javascript">
    var datatableC_twitter = new Datatable();
    datatableC_twitter.init({
        src: $("#datatableC_twitter"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('dashboard/c_twitter/getDatatable/') ?>", // ajax source
            },
            "order": [
                [1, "asc"]
            ],// set first column as a default sort by asc
        }
    });
</script>