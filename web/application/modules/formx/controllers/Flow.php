<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Flow extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_flow');
        $this->load->model('M_form');
        $this->load->model('M_form_param');
        $this->load->model('Formx_model');
        $this->load->model('T_flow_model');
        $this->load->model('M_flow_level_model');
        $this->load->model('T_flow_history_model');

        $this->load->library('form_validation');

        $this->data['authorize'] = 'write';

    }

    
    public function table($flow_id=1)
	{
        $this->data['menu_kode'] = 'flow_'.$flow_id;
		$level=1;
        $data['flow_id'] = $flow_id;
        if ($data['m_flow'] = $this->M_flow->get($flow_id)) {
    		$form_id = $data['m_flow']->form_id;

            $data['usergroup_creator'] = $this->M_flow_level_model->usergroup_creator($flow_id);
            $data['usergroup_id'] = $this->usergroup_id;


            $m_form =$this->M_form->get($form_id);
            $data['form'] = $m_form;
            $data['form_id'] = $m_form->id ;

            $data['form_param'] = $this->Formx_model->get_param_datatable($form_id);
            $this->template('v_flow_list', $data);
        }else{
            show_404();
        }
	}

    public function getDatatable()
    {
        $flow_id = $this->input->post('flow_id');
        $form_id = $this->input->post('form_id');
        $flow = $this->M_flow->get($flow_id);
        $form_param = $this->Formx_model->get_param_datatable($form_id);

        $customActionName=$this->input->post('customActionName');
        $records         = array();

        if ($customActionName == "delete") {
            $records=$this-> delete_checked();
        }

        $iDisplayLength = ($this->input->get_post('length')) ? $this->input->get_post('length') : 10 ;
        $iDisplayStart  = ($this->input->get_post('start')) ? $this->input->get_post('start') : 0 ;
        $sEcho          = ($this->input->get_post('draw')) ? $this->input->get_post('draw') : 1 ;

        $this->Formx_model->set_table($form_id);
        $t              = $this->Formx_model->get_limit_data($iDisplayStart, $iDisplayLength);
        $iTotalRecords  = $t['total_rows'];
        $get_data       = $t['get_db'];

        $records["data"] = array();

        $i=$iDisplayStart+1;
        if ($get_data) {
            foreach ($get_data as $d) {
                $checkbok= '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="group-checkable" name="id[]" value="'.$d->id.'"><span></span></label>';
                $view  = '<a class="btn btn-icon-only green" title="view"  href="'.site_url('formx/flow/read/'.$flow_id.'/'.$d->t_flow_id).'"><i class="fa fa-search fa-lg"></i></a>';
                $edit  = '<a class="btn btn-icon-only blue" title="edit"  href="'.site_url('formx/flow/form/'.$flow_id.'/'.$d->t_flow_id).'"><i class="fa fa-pencil-square-o fa-lg"></i></a>';
                $delete  = '<button class="btn btn-icon-only red delete" title="delete" data-title="" data-url="'.site_url('formx/flow/delete/'.$form_id.'/'.$d->id).'"><i class="fa fa-trash fa-lg"></i></button>';
                $approve  = '<a class="btn btn-icon-only yellow openmodal" title="approve"  href="'.site_url('formx/flow/approve/'.$flow_id.'/'.$d->t_flow_id).'"><i class="fa fa-check fa-lg"></i></a>';
                $attch  = '<a class="btn btn-icon-only default openmodal" title="support view"  href="'.site_url('formx/attchment/get_file/'.$d->t_flow_id).'"><i class="fa fa-file fa-lg"></i></a>';


                $row = array();
                $row[] =$i++;
                foreach ($form_param->result() as $p) {
                    if ($p->type == 'select') {
                        if (!empty($d->{$p->column_name})) {
                            $row[] = $this->Formx_model->get_value_dd($p->table_ref,$d->{$p->column_name});
                        } else{
                            $row[]='';
                        }
                        
                    }elseif ($p->type == 'upload') {
                        continue;
                    }elseif ($p->type == 'file'||$p->type == 'img') {
                        $row[] = '<a href="'. base_url($p->path_upload.$d->{$p->column_name}).'" class="btn blue"><i class="fa fa-download"></i></a>';
                    }elseif($p->type == 'date') {
                        $row[] = date_to_indonesia($d->{$p->column_name});
                    } else {
                        $row[] = $d->{$p->column_name};
                    }
                    
                }

                $flow_level=null;
                $status = '';
                if (!empty($d->t_flow_id)) {
                    if($t_flow = $this->T_flow_model->get($d->t_flow_id)){

                        $w = array(
                        	'level_no' => $t_flow->level_no, 
                        	'flow_id' => $t_flow->flow_id, 
                        );
                        if ($flow_level = $this->M_flow_level_model->get($w)) {
        	                $status = '<a class="openmodal btn yellow" href="#" data-url="'.site_url('formx/history/note/'.$d->t_flow_id).'">'.$flow_level->level_no.' - '.$flow_level->level_name.'</a>';
                        } 
                    }
                }
                $row[]=$status;
                
                $btn_action = '';
                $is_allowed_edit = false;
                if ($flow_level) {
                    $max_min = $this->M_flow_level_model->get_max_min($flow_id);
                    if ($max_min->max > $t_flow->level_no) {
                        if ($flow_level->to_user) {
                            if($t_flow->to_user == $this->user_id){
                                $is_allowed_edit = true;
                            }
                        } else if(in_array( $this->usergroup_id,explode(',', $flow_level->usergroup_id))) {
                            $is_allowed_edit = true;
                        } else if(empty($flow_level->usergroup_id) && $t_flow->created_by==$this->user_id){
                            $is_allowed_edit = true;
                        }
                    }
                    
                } 
                if ($is_allowed_edit) {
                    $btn_action = $edit;
                    $btn_action .= $approve;
                }

                if (!empty($flow->btn)) {
                    $btn_action .= $flow->btn;
                    
                }

                
                $row[] = $btn_action.$view.$attch;
                $records["data"][] = $row;
            }
        }
        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        $this->output->set_content_type('application/json')->set_output(json_encode($records));
    }

    public function read($flow_id=null,$t_flow_id=null)
    {
        $this->data['authorize'] = 'view';
        $this->form($flow_id,$t_flow_id);
    }

    public function form($flow_id=null,$t_flow_id=null)
    {
        $this->data['menu_kode'] = 'flow_'.$flow_id;
        
        if (!empty($t_flow_id)) {
            if($t_flow = $this->T_flow_model->get($t_flow_id)){
                $flow_id = $t_flow->flow_id;
                $level_no = $t_flow->level_no;
            }else{
                show_error('Form not found');
            }
        } else {
            $data_new_flow = array(
                'flow_id' => $flow_id, 
                'level_no' => 1,
                'created_by' =>$this->user_id 
            );
            $t_flow_id = $this->T_flow_model->insert($data_new_flow);
            $flow_id = $flow_id;
            $level_no = 1;
        }


        $this->db->where('id', $flow_id);
        $m_flow = $this->db->get('m_flow');
        if ($m_flow->num_rows() > 0) {
            $data['flow'] = $m_flow->row();
            
            $w = array(
                'level_no' => $level_no,
                'flow_id' => $flow_id,
                 );
            $flow_level = $this->M_flow_level_model->get($w);
            $w = array(
                'level_no' => $level_no+1,
                'flow_id' => $flow_id,
                 );
            $data['flow_level_next'] = $this->M_flow_level_model->get($w);

            if ($this->data['authorize'] == 'view') {
                $data['form_write_arr'] = array();
                $data['form_read_arr'] = array_merge(
                                             array_filter(explode(',', $flow_level->form_id)),
                                             array_filter(explode(',', $flow_level->form_id_read))
                                         );
                $data['form_many_write_arr'] = array();
                $data['form_many_read_arr'] = array_merge(
                                        array_filter(explode(',', $flow_level->form_many)),
                                        array_filter(explode(',', $flow_level->form_many_read))
                                    );
            }else{
                
                $data['form_write_arr'] = array_filter(explode(',', $flow_level->form_id));
                $data['form_read_arr'] = array_filter(explode(',', $flow_level->form_id_read));
                $data['form_many_write_arr'] = array_filter(explode(',', $flow_level->form_many));
                $data['form_many_read_arr'] = array_filter(explode(',', $flow_level->form_many_read));
            }
            
            $data['max_min'] = $this->M_flow_level_model->get_max_min($flow_id);
            $data['level_no']=$level_no;

            // $data['form_param'] = $this->Formx_model->get_param($form_id);
            $data['action']    = site_url('formx/formx/form_action');
            $data['t_flow_id'] = $t_flow_id;
            $data['flow_id'] = $flow_id;


            $this->template('v_flow_form', $data);
        }else{
            show_error('Form not found');
        }
    }

    // public function form_action()
    // {
    //     $res['success'] = false;
    //     $res['message'] = 'Simpan gagal';

    //     $form_id = $this->input->post('form_id');
    //     $id = $this->input->post('id');
    //     $this->Formx_model->set_table($form_id);

    //     $form_param= $this->Formx_model->get_param($form_id);
        
    //     foreach ($form_param->result() as $p) {
    //         $validation_arr = array('trim');
    //         if ($p->type == 'int') {
    //             $validation_arr[] = 'integer';
    //         }
    //         if ($p->required) {
    //             $validation_arr[] = 'required';
    //         }
    //         if (!empty($p->validation)) {
    //             $validation_arr[] = $p->validation;
    //         }
    //         $validation = implode('|', $validation_arr);
    //         $this->form_validation->set_rules($p->column_name,$p->column_name,$validation);
    //     }

    //     if ($this->form_validation->run() == FALSE) {
    //         $res['message'] = 'Lengkapi form dengan benar';
    //         $res['field_error'] = $this->form_validation->error_array();
    //     } else {

    //         $t_flow_id = $this->input->post('t_flow_id');
    //         $flow_id = $this->input->post('flow_id');
    //         if (empty($t_flow_id)) {
    //             $data = array(
    //                 'flow_id' => $flow_id, 
    //                 'level_no' => 1, 
    //             );
    //             $t_flow_id = $this->T_flow_model->insert($data);
    //         } 
    //         $res['t_flow_id'] = $t_flow_id;

    //         $data = array('t_flow_id'=>$t_flow_id);
    //         foreach ($form_param->result() as $p) {
    //             if ($p->type == 'upload') continue;

    //             if ($p->type == 'int' || $p->type == 'date') {
    //                 if (!empty($this->input->post($p->column_name))) {
    //                     $data[$p->column_name] = $this->input->post($p->column_name);
    //                 } else {
    //                     $data[$p->column_name] = NULL;
    //                 }
    //             }else{
    //                 $data[$p->column_name] = $this->input->post($p->column_name);
    //             }
    //         }


    //         if (empty($id)) {
    //             if($id = $this->Formx_model->insert($data)){
    //                 if ($id_temp = $this->input->post('id_temp')) {
    //                     $temp_folder= "./uploads/temp/formx/".$id_temp;
    //                     $target_folder = "./uploads/formx/".$id;
    //                     if(file_exists($temp_folder)) {
    //                         mkdir($target_folder,0775,true);
    //                         rename($temp_folder, $target_folder);
    //                     }
    //                 }
    //                 // $res['url']     = site_url('formx/flow/form/'.$flow_id.'/'.$t_flow_id);
    //                 $res['id'] = $id;
    //                 $res['success'] = true;
    //                 $res['message'] = 'Simpan berhasil';
    //             }
    //         }else{
    //             $row = $this->Formx_model->get($id);

    //             if ($row) {
    //                 if($this->Formx_model->update($data,$id)){
    //                     // $res['url']     = site_url('formx/flow/table/'.$form_id);
    //                     $res['id'] = $id;
    //                     $res['success'] = true;
    //                     $res['message'] = 'Simpan berhasil';
    //                 }
    //             } else {
    //                 $res['success'] = false;
    //                 $res['message'] = 'Data not found';
    //             }
    //         }

    //     }
    //     $this->output->set_content_type('application/json')->set_output(json_encode($res));
    // }

    public function delete($form_id,$id)
    {
        $res['success'] = false;
        $res['message'] = 'Hapus gagal';
        
        $this->Formx_model->set_table($form_id);

        $row = $this->Formx_model->get($id);

        if ($row) {
            $this->Formx_model->delete($id);
            $res['success'] = true;
            $res['message'] = 'Hapus berhasil';
        } else {
            $res['message'] = 'Data tidak ditemukan';
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function delete_checked()
    {
        $id_array=$this->input->post('id[]');
        foreach ($id_array as $id) {
            $row = $this->Formx_model->get($id);

            if ($row) {
                $this->Formx_model->delete($id);
            }
        }
        $result["customActionStatus"]="OK";
        $result["customActionMessage"]="Delete Record Success";
        return $result;
    }

    public function approve($flow_id=null,$t_flow_id=null)
    {
        $data['t_flow_id'] = $t_flow_id;
        $data['flow_id'] = $flow_id;

        if (!empty($t_flow_id)) {
            if($t_flow = $this->T_flow_model->get($t_flow_id)){
                $flow_id = $t_flow->flow_id;
                $level_no = $t_flow->level_no;
            }else{
                show_error('Form not found');
            }
        }

        $w = array(
                'level_no' => $level_no+1,
                'flow_id' => $flow_id,
                 );
        $data['flow_level_next'] = $this->M_flow_level_model->get($w);

        $data['max_min'] = $this->M_flow_level_model->get_max_min($flow_id);
        $data['level_no']=$level_no;
        $this->template('v_approve',$data);
    }

    public function update_level()
    {
        $res['success'] = false;
        $res['message'] = 'Simpan gagal';

        $flow_id=$this->input->post('flow_id');
        $t_flow_id=$this->input->post('t_flow_id');
        if (empty($t_flow_id)) {
            $t_flow_id = NULL;
            $from_level_no = 1 ;
        }else{
            $t_flow = $this->T_flow_model->get($t_flow_id);
            $from_level_no = $t_flow->level_no;
        }
        
        $catatan      =$this->input->post('catatan');
        $action       =$this->input->post('action');
        $target_level =$this->input->post('target_level');
        $to_user      = (!empty($this->input->post('to_user'))) ? $this->input->post('to_user') : NULL;
        
        if (empty($target_level)) {
            if ($action == "ekposisi") {
                $level_no = $from_level_no+1;
            }else if ($action == "disposisi") {
                $level_no = $from_level_no-1;
                $w = array(
                    'level_no' => $level_no,
                    'flow_id' => $flow_id,
                     );
                $flow_level_target = $this->M_flow_level_model->get($w);
                if($flow_level_target->to_user){
                    $w = array(
                        't_flow_id' => $t_flow_id,
                        'from_level_no' => $level_no,
                         );
                    $this->db->order_by('id', 'desc');
                    $history = $this->T_flow_history_model->get($w);
                    $to_user = $history->user_id;
                }
            }
        }

        $data = array(
            'user_id'          => $this->user_id, 
            'approval_catatan' => $catatan, 
            'approval_status'  => $action, 
            'approval_date'    => date('Y-m-d h:i:s'), 
            'level_no'         => $level_no, 
            'from_level_no'    => $from_level_no, 
            'flow_id'          => $flow_id, 
            'to_user'          => $to_user, 
        );
        $w = array(
            'id' => $t_flow_id, 
        );

        if ($a=$this->T_flow_model->replace($data,$w) != false) {
            $data['t_flow_id'] = $t_flow_id;
            $this->T_flow_history_model->insert($data);

            $res['url']     = site_url('formx/flow/table/'.$flow_id);
            $res['success'] = true;
            $res['message'] = 'Simpan berhasil';
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($res));

    }

}
