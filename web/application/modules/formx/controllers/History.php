<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->model('T_flow_history_model');
		$this->load->model('M_flow_level_model');
		$this->load->model('M_form');
		$this->load->model('M_form_param');
		$this->load->model('Formx_model');
	}

	public function note($t_flow_id)
	{
		$this->T_flow_history_model->table = 'v_t_flow_history';
		$w = array('t_flow_id' => $t_flow_id );
		$this->db->order_by('id', 'asc');
		$data['history'] = $this->T_flow_history_model->get_all($w);
		$data['t_flow_id'] = $t_flow_id;
		$this->load->view('v_flow_history',$data);
	}

}

/* End of file History.php */
/* Location: ./application/modules/formx/controllers/History.php */