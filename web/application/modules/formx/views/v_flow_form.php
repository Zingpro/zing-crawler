        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->

                <!-- Main content -->
                <section class='content'>
                  <div class='row'>
                    <div class='col-md-12'>
                      <div class='portlet light'>
                        <div class='portlet-title'>
                          <div class='caption font-green'>
                            <a href="<?php echo site_url('formx/flow/table/'.$flow_id) ?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back</a>
                            <span class='caption-subject bold uppercase'><?php echo $flow->name ?> </span>
                          </div>

                          <?php if ($authorize == 'write'): ?>
                          <div class="actions">
                            <?php if ($max_min->min < $level_no): ?>
                            <div class="btn-group" >
                              <a class="btn btn-circle red btn_do" data-toggle="modal" data-action="disposisi" href='#modal-flow-catatan'>← Prev</a>
                            </div>
                            <?php endif ?>
                            <?php if ($max_min->max > $level_no): ?>
                            <div class="btn-group" id="next_btn" <?php echo (empty($t_flow_id)) ? 'style="display:none"' : "" ; ?> >
                              <a class="btn btn-circle green btn_do" data-toggle="modal" data-action="ekposisi" href='#modal-flow-catatan'>Next →</a>
                            </div>
                            <?php endif ?>
                          </div>
                          <?php endif ?>

                        </div>
                        <div class='portlet-body'>
                          <?php $this->load->view('v_flow_tab_read'); ?>
                          <!-- <div class="col-md-12">
                            <div class="note note-info">
                              <h4 class="block">Form Input</h4>
                            </div>
                          </div> -->
                          <div class='row'>
                            <?php foreach ($form_write_arr as $form_id): ?>
                            <?php if (!empty($form_id)): ?>  
                            <?php 
                              $form = $this->M_form->get($form_id);
                              $form_param = $this->Formx_model->get_param($form_id );

                              $row = $this->Formx_model->get_row_by_flow_id($form_id,$t_flow_id);
                              if ($row) {
                                  $row       = $row;
                                  $id        = $row->id;
                              } else {
                                  $row       = null;
                                  $id        = '';
                              }
                              $w = array('parent_form_id' => $form_id );
                              $data['form_many'] = $this->M_form->get_all($w);
                              
                              $data['row'] = $row;
                              $data['id'] = $id;
                              $data['form_id'] = $form_id;
                              $data['form'] = $form;
                              $data['form_param'] = $form_param;
                              $data['flow_id'] = $flow_id;
                              $data['t_flow_id'] = $t_flow_id;
                              $data['authorize'] = "write";

                            ?>  
                            <div class='col-md-12'>
                              <?php $this->load->view('v_form',$data); ?>
                            </div>
                            <?php endif ?>
                            <?php endforeach ?>
                          </div>
                          <div class="row">
                            <?php foreach ($form_many_write_arr as $form_id): ?>
                            <?php if (!empty($form_id)): ?>  

                              <div id="form_table_<?php echo $form_id?>"></div>
                              <script>
                                $('#form_table_<?php echo $form_id?>').load('<?php echo site_url("formx/formx/table/".$form_id) ?>',
                                  {
                                    parent_id: "<?php echo $t_flow_id ?>",
                                    parent_column: "t_flow_id",
                                    flow_id: "<?php echo $flow_id ?>",
                                    view_only:"true"
                                  } ,
                                  function(){
                                  /* Stuff to do after the page is loaded */
                                });
                                
                              </script>
                            <?php endif ?>
                            <?php endforeach ?>
                          </div>
                        </div>
                      </div><!-- /.box -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </section><!-- /.content -->

            </div>
        </div>

<div class="modal fade" id="modal-flow-catatan">
  <div class="modal-dialog">
    <form action="<?php echo site_url('formx/flow/update_level') ?>" method="post" id="form-catatan">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Catatan</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="t_flow_id" value="<?php echo $t_flow_id ?>" class="t_flow_id">
        <input type="hidden" name="flow_id" value="<?php echo $flow_id ?>">
        <input type="hidden" name="action" id="modal_action" value="">
        <div class="form-group">
            <label>Catatan</label>
            <textarea class="form-control" rows="10" class="form-control" name="catatan"></textarea>
        </div>

        <div class="form-group" id='field_to_user'>
          <?php if ($flow_level_next->to_user): ?>
            <label>User</label>
            <select name='to_user' class='form-control select2-ajax' data-url='<?php echo site_url('formx/dropdown/user') ?>'>
            </select>
          <?php endif ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
    </form>
  </div>
</div>
<script type="text/javascript">
  $('.input_form').submit(function(e) {
        e.preventDefault();
        main.submitAjaxModal($(this));
        // var options = {'f_response':'update_t_flow_id'};
        // main.submitAjaxModal($(this),options);
  });
  $('#form-catatan').submit(function(e) {
        e.preventDefault();
        main.submitAjaxModal($(this));
  });
  $('.btn_do').click(function(event) {
    if ($(this).data('action') == "ekposisi") {
      $('#field_to_user').show();
    }else{
      $('#field_to_user').hide();
    }
    $('#modal_action').val($(this).data('action'));
  });

  function update_t_flow_id(response,formObj) {
    if (response.t_flow_id) {
      $('.t_flow_id').val(response.t_flow_id);
      $("input[name='id']", formObj).val(response.id);
      $('#next_btn').show();
    }
  }
</script>
