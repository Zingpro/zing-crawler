

<div id="catatan_perizinan" class="modal fade modal-scroll" tabindex="-1" data-replace="true" data-backdrop="static" data-keyboard="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">History</h4>
			</div>
			<div class="modal-body">
				<div class="portlet light">
					<div class="portlet-body">
						<div class="timeline">
						<?php if ($history): ?>
							<?php foreach ($history as $row): ?>
							<?php 
								$w = array(
									'flow_id' => $row->flow_id, 
									'level_no' => $row->level_no, 
								);
								$level = $this->M_flow_level_model->get($w);
							 ?>
	                        <!-- TIMELINE ITEM -->
	                        <div class="timeline-item">
	                            <div class="timeline-badge">
	                                <!-- <img class="timeline-badge-userpic" src="../assets/pages/media/users/avatar80_1.jpg"> -->
	                                <div class="timeline-icon">
                                        <i class="icon-user-following font-green-haze"></i>
                                    </div>
                                </div>
	                            <div class="timeline-body">
	                                <div class="timeline-body-arrow"> </div>
	                                <div class="timeline-body-head">
	                                    <div class="timeline-body-head-caption">
	                                        <a href="javascript:;" class="timeline-body-title font-blue-madison"><?php echo $row->level_name ; ?></a>
	                                        <span class="timeline-body-time font-grey-cascade"><?php echo $row->approval_date ?></span>
	                                    </div>
	                                </div>
	                                <div class="timeline-body-content">
	                                    <span class="font-grey-cascade"> <?php echo $row->approval_catatan ?> </span>
	                                    <div>
	                                    	<?php 
	                                    		// $w = array(
	                                    		// 	'flow_id' => $row->flow_id,
	                                    		// 	'level_no' => $row->from_level_no,
	                                    		// 	 );
	                                    		// $flow_level = $this->M_flow_level_model->get($w);

	                                    		// $data_flow_level['form_read_arr'] = explode(',', $flow_level->form_id);
	                                    	 //  	$this->load->view('v_flow_tab_read',$data_flow_level,FALSE); 
                                    	  	?>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <!-- END TIMELINE ITEM -->
							<?php endforeach ?>
						<?php endif ?>
                        </div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn">Tutup</button>
			</div>
		</div>
	</div>
</div>
<script>
	$("#catatan_perizinan").modal('show');
</script>
