        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->

                <!-- Main content -->
                <section class='content'>
                  <div class='row'>
                    <div class='col-md-12'>
                      <div class='portlet light portlet-fit portlet-datatable bordered'>
                        <div class='portlet-title'>
                            <div class="caption">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase"><?php echo $form->form_name ?>  </span>
                            </div>
                            <div class="actions">
                                <?php //if (in_array('xcreate', $ar_haklistakses)): ?>
                                    <?php if ( in_array($usergroup_id, $usergroup_creator)  || empty($usergroup_creator)): ?>
                                    <div class="btn-group" >
                                            <?php echo anchor('formx/flow/form/'.$flow_id,'<i class="fa fa-pencil"></i> Create',array('class'=>'btn btn-circle btn-info btn-sm'));?>
                                    </div>
                                    <?php endif ?>
                                <?php //endif ?>
                                <div class="btn-group">
                                    <a class="btn red btn-circle" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-share"></i>
                                        <span class="hidden-xs"> Tools </span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right" id="datatable_ajax_tools">
                                        <li>
                                            <a href="javascript:;" data-action="0" class="tool-action">
                                                <i class="icon-printer"></i> Print</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="1" class="tool-action">
                                                <i class="icon-check"></i> Copy</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="2" class="tool-action">
                                                <i class="icon-doc"></i> PDF</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="3" class="tool-action">
                                                <i class="icon-paper-clip"></i> Excel</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" data-action="4" class="tool-action">
                                                <i class="icon-cloud-upload"></i> CSV</a>
                                        </li>
                                        <li class="divider"> </li>
                                        <li>
                                            <a href="javascript:;" data-action="5" class="tool-action">
                                                <i class="icon-refresh"></i> Reload</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div><!-- /.box-header -->
                        <div class='portlet-body'>
                            <div class='table-container'>
                                <!-- <div class="table-actions-wrapper">
                                    <span> </span>
                                    <select class="table-group-action-input form-control input-inline input-small input-sm">
                                        <option value="">Select...</option>
                                        <option value="delete">Delete</option>
                                    </select>
                                    <button class="btn btn-sm green table-group-action-submit">
                                        <i class="fa fa-check"></i> Submit</button>
                                </div> -->
                                <table class="table table-striped table-bordered table-hover" id="mytable">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <!-- <th width="2%"><input type="checkbox" class="group-checkable"> </th> -->
                                            <th>No</th>

                                            <?php foreach ($form_param->result() as $p): ?>
                                            <?php if ($p->type == 'upload') continue; ?>

                                            <th>
                                                <?php
                                                if (!empty($p->label_name)) {
                                                    echo $p->label_name;
                                                }else{
                                                    echo $p->column_name;
                                                }
                                                ?>
                                            </th>
                                            <?php endforeach ?>
                                            <th>Status</th>
                                            <th class="not-export-column">Action</th>
                                        </tr>
                                        <tr role="row" class="filter">
                                            <td></td>
                                            <?php foreach ($form_param->result() as $p): ?>
                                                <?php if ($p->type == 'upload') continue; ?>
                                                <?php if ($p->type == 'int'): ?>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm" name="<?php echo $p->column_name ?>_min" placeholder="min">
                                                        <input type="text" class="form-control form-filter input-sm" name="<?php echo $p->column_name ?>_max" placeholder="max">
                                                    </td>
                                                <?php elseif ($p->type == 'date'): ?>
                                                    <td>
                                                        <input type="text" class="form-control form-filter input-sm date-picker" name="<?php echo $p->column_name ?>_min" placeholder="min">
                                                        <input type="text" class="form-control form-filter input-sm date-picker" name="<?php echo $p->column_name ?>_max" placeholder="max">
                                                    </td>
                                                <?php elseif ($p->type == 'select'): ?>
                                                    <td>
                                                        <select name='<?php echo $p->column_name ?>' class='form-control form-filter select2-ajax' data-url='<?php echo site_url('formx/dropdown/dd/'.$p->table_ref) ?>'></select>
                                                    </td>
                                                <?php else: ?>
                                                    <td><input type="text" class="form-control form-filter input-sm" name="<?php echo $p->column_name ?>"></td>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                            <td></td>
                                            <td>
                                                <div class="margin-bottom-5">
                                                    <!-- <button class="btn btn-sm green btn-outline filter-submit margin-bottom"> -->
                                                    <i class="fa fa-search filter-submit"></i> 
                                                    <!-- Search</button> -->
                                                    <!-- <button class="btn btn-sm red btn-outline filter-cancel"> -->
                                                    <i class="fa fa-times filter-cancel"></i> 
                                                    <!-- Reset</button> -->
                                                </div>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div><!-- /.table-container -->
                        </div><!-- /.portlet-body -->
                      </div><!-- /.portlet -->
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </section><!-- /.content -->

            </div>
        </div>

<script type="text/javascript">
$(document).ready(function() {

    var datatableAjax = new Datatable();
    datatableAjax.setDefaultParam("form_id","<?php echo $form_id ?>");
    datatableAjax.setDefaultParam("flow_id","<?php echo $flow_id ?>");
    datatableAjax.init({
        src: $("#mytable"),
        dataTable: {
            "ajax": {
                "url": "<?php echo site_url('formx/flow/getDatatable/') ?>", // ajax source
            },
            "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                'orderable': false,
                'targets': [-1]
            }],
            "order": [
                [0, "desc"]
            ],// set first column as a default sort by asc
        }
    });
});

</script>
