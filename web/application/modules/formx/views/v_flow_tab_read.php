<?php //if (count($form_read_arr)>0||count($form_many_read_arr)>0): ?>
<!-- <div class="note note-warning">
    <h4 class="block">Data Info! </h4>
</div> -->
<?php //endif ?>
<div class="tabbable-line">
  <ul class="nav nav-tabs ">
    <!-- normal form -->
    <?php $tab_index = 0; ?>
    <?php foreach ($form_read_arr as $form_id): ?>
      <?php if (!empty($form_id)): ?>
      <?php 
        $form = $this->M_form->get($form_id);
      ?> 
      <li class="<?php echo ($tab_index==0) ? 'active' :'' ?>">
          <a href="#tab_read_<?php echo $tab_index ?>" data-toggle="tab"> <?php echo $form->form_name ?> </a>
      </li>
    <?php $tab_index++ ?>
    <?php endif ?>
    <?php endforeach ?>
    <!-- sub form -->
    <?php foreach ($form_many_read_arr as $form_id): ?>
      <?php if (!empty($form_id)): ?>
      <?php 
        $form = $this->M_form->get($form_id);
      ?> 
      <li class="<?php echo ($tab_index==0) ? 'active' :'' ?>">
          <a href="#tab_read_<?php echo $tab_index ?>" data-toggle="tab"> <?php echo $form->form_name ?> </a>
      </li>
    <?php $tab_index++ ?>
    <?php endif ?>
    <?php endforeach ?>
  </ul>
  <div class="tab-content">
    <?php $tab_index = 0; ?>
    <?php foreach ($form_read_arr as $form_id): ?>
      <?php if (!empty($form_id)): ?>
      <?php 
        $form = $this->M_form->get($form_id);
        $form_param = $this->Formx_model->get_param($form_id );
        $row = $this->Formx_model->get_row_by_flow_id($form_id,$t_flow_id);
        if ($row) {
            $row       = $row;
            $id        = $row->id;
        } else {
            $row       = null;
            $id        = '';
        }
        $w = array('parent_form_id' => $form_id );
        $data['form_many'] = $this->M_form->get_all($w);
        
        $data['row'] = $row;
        $data['id'] = $id;
        $data['form_id'] = $form_id;
        $data['form'] = $form;
        $data['form_param'] = $form_param;
        $data['flow_id'] = $flow_id;
        $data['t_flow_id'] = $t_flow_id;
        $data['authorize'] = "readonly";
      ?>  
      <div class="tab-pane <?php echo ($tab_index==0) ? 'active' :'' ?>" id="tab_read_<?php echo $tab_index++ ?>">
        <div class='col-md-12'>
          <?php $this->load->view('v_form',$data); ?>
        </div>
        <div class="col-md-12"></div>
      </div>
      <?php endif ?>
    <?php endforeach ?>
    <?php foreach ($form_many_read_arr as $form_id): ?>
      <?php if (!empty($form_id)): ?>  
        <div class="tab-pane <?php echo ($tab_index==0) ? 'active' :'' ?>" id="tab_read_<?php echo $tab_index++ ?>">
          <div id="form_table_<?php echo $form_id?>"></div>
          <script>
            $('#form_table_<?php echo $form_id?>').load('<?php echo site_url("formx/formx/table/".$form_id) ?>',
              {
                parent_id: "<?php echo $t_flow_id ?>",
                parent_column: "t_flow_id",
                flow_id: "<?php echo $flow_id ?>",
                view_only:"true",
                authorize:"readonly",
              } ,
              function(){
              /* Stuff to do after the page is loaded */
            });
            
          </script>
        </div>
      <?php endif ?>
    <?php endforeach ?>
  </div>
</div>