<?php
  $tag_form_upload = '';
  foreach ($form_param->result() as $p) {
    if($p->type == 'img' || $p->type == 'file'){
      $tag_form_upload = 'enctype="multipart/form-data"';
      break;
    }
  }
?>
<div class='portlet light'>
  <div class='portlet-title'>
    <div class='caption font-green'>
      <span class='caption-subject bold uppercase'><?php echo $form->form_name ?> </span>
    </div>
  </div>
  <div class='portlet-body form'>
    <div class="form">
      <form action="<?php echo $action; ?>" method="post" id="input_form_<?php echo $form_id; ?>" class="horizontal-form <?php echo $authorize ?>" <?php echo $tag_form_upload ?>>
          <div class='form-body'>
            <div class='row'>
              <?php foreach ($form_param->result() as $p): ?>
              <?php
                $col_md_param = 'col-md-6';
                if (!empty($p->col_md)) {
                  $col_md_param = $p->col_md;
                }
                $readonly ='';
                if ($p->readonly) {
                  $readonly = 'readonly';
                }
                $default_value='';
                if (!empty($p->default_value)) {
                  $default_value=$p->default_value;
                }
              ?>
              <div class='<?php echo $col_md_param ?>'>
                <div class='form-group'>
                  <label class='control-label'><?php
                      if (!empty($p->label_name)) {
                          echo $p->label_name;
                      }else{
                          echo $p->column_name;
                      } ?>
                  </label>
                    <!-- ---------------------------------------------------- -->
                    <?php if ($p->type == 'date'):
                      if ($p->default_value=='now')
                        $default_value = date("Y-m-d");

                      ?>
                      <input type="text" class="form-control date-picker" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>"  value="<?php echo (isset($row)) ? date_to_indonesia($row->{$p->column_name}) : date_to_indonesia($default_value); ?>" placeholder="dd-mm-yyyy"/>

                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'textarea'): ?>
                      <textarea class="form-control" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>" cols="30" rows="5"><?php echo (isset($row)) ? $row->{$p->column_name} : $default_value; ?></textarea>

                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'select_ajax'): ?>
                      <?php
                      $uom_id = (isset($row)) ? $row->{$p->column_name} : $default_value;
                      if (!empty($uom_id)) {
                        $uom_id_name = $this->Formx_model->get_value_dd($p->table_ref,$uom_id);
                      }else{
                        $uom_id_name = '';
                      }
                      ?>
                      <!-- <div class="input-group"> -->
                      <?php if ($readonly != 'readonly'): ?>
                        <select name='<?php echo $p->column_name ?>' class='form-control select2-ajax' data-url='<?php echo site_url('formx/dropdown/dd/'.$p->table_ref) ?>'>
                          <option value="<?php echo $uom_id; ?>" selected="selected"><?php echo $uom_id_name; ?></option>
                        </select>
                        <?php if (!empty($p->new_form_id)): ?>
                        <span class="input-group-btn">
                          <button type="button" class="btn blue openmodal" data-url="<?php echo site_url('formx/formx/form/'.$p->new_form_id) ?>" data-id="div_modal_form_<?php echo $p->new_form_id?>"><i class="fa fa-plus"></i></button>
                        </span>
                        <!-- <script>
                          var $newOption = $("<option></option>").val("TheID").text("The text");
                          $("#param_id").html($newOption).trigger('change');
                        </script> -->
                        <?php endif ?>
                      <?php else: ?>
                        <select name='<?php echo $p->column_name ?>' class='form-control' readonly>
                          <option value="<?php echo $uom_id; ?>" selected="selected"><?php echo $uom_id_name; ?></option>
                        </select>
                      <?php endif ?>
                      <!-- </div> -->
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'select'): ?>
                      <?php
                        if(!$options = json_decode($p->table_ref,true)){
                          $options = $this->Formx_model->get_array_dd($p->table_ref);
                        }
                        $data_attribut = array(
                                'class'       => 'form-control select2me',
                                'data-allow-clear' => 'true'
                        );
                        $options[""]="";
                        $dd_value = (isset($row)) ? $row->{$p->column_name} : $default_value;
                        echo form_dropdown($p->column_name, $options, $dd_value,$data_attribut);
                       ?>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'radio'): ?>
                      <?php
                        if(!$options = json_decode($p->table_ref,true)){
                          $options = $this->Formx_model->get_array_dd($p->table_ref);
                        }
                        $data_attribut = array(
                                'class'       => 'form-control',
                        );
                        echo '<div class="mt-radio-inline">';
                        foreach ($options as $key => $value) {
                          $checked = (isset($row) && $row->{$p->column_name} == $key ) ? 'checked' : '';
                          echo '
                            <label class="mt-radio mt-radio-outline"> '.$value.'
                                <input type="radio" name="'.$p->column_name.'" value="'.$key.'" '.$checked.' >
                                <span></span>
                            </label>';
                        }
                        echo "</div>";
                       ?>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'checkbox'): ?>
                      <div class="mt-checkbox-list">
                        <?php
                          if(!$options = json_decode($p->table_ref,true)){
                            $options = $this->Formx_model->get_array_dd($p->table_ref);
                          }
                          $dd_value = (isset($row)) ? json_decode($row->{$p->column_name}) : [];
                          if($dd_value == NULL)$dd_value = [];
                        ?>
                        <?php foreach ($options as $key => $value): ?>
                          <label class="mt-checkbox mt-checkbox-outline"> <?php echo $value ?>
                              <input value="<?php echo $key ?>" name="<?php echo $p->column_name ?>[]" type="checkbox" <?php echo (in_array($key, $dd_value)) ? "checked" : "" ; ?>>
                              <span></span>
                          </label>
                        <?php endforeach ?>
                      </div>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'upload'): ?>
                      <?php
                      if (empty($id)&&empty($id_temp)) {
                        $id_temp =uniqid();
                        echo form_hidden('id_temp', $id_temp);
                      }
                      ?>

                      <?php
                      if (!empty($id)) {
                        $dt = 'formx/'.$id.'/'.$p->id;
                      }else{
                        $dt = 'temp/formx/'.$id_temp.'/'.$p->id;
                      }
                      $this->load->view('dropzone/v_dropzone_init', array('dt' => $dt,'dz_readonly'=>$authorize), FALSE);
                      ?>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'file'): ?>
                      <div>
                          <?php if (isset($row) && !empty($row->{$p->column_name})): ?>
                            <a href="<?php echo base_url($p->path_upload.$row->{$p->column_name}) ?>" class="btn blue"><i class="fa fa-download"></i></a>
                          <?php endif ?>
                          <?php if ($authorize != 'readonly'): ?>
                          <div class="fileinput fileinput-new" data-provides="fileinput">
                              <span class="btn green btn-file">
                                  <span class="fileinput-new"> Select file </span>
                                  <span class="fileinput-exists"> Change </span>
                                  <input type="file" name="<?php echo $p->column_name; ?>" id="<?php echo $p->column_name; ?>"> </span>
                              <span class="fileinput-filename"> </span> &nbsp;
                              <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                          </div>
                          <?php endif ?>

                      </div>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'img'): ?>
                      <div>
                        <div class="fileinput fileinput-new <?php echo 'data-'.$authorize ?>" data-provides="fileinput">
                            <?php if (isset($row)): ?>
                            <div class="fileinput-new thumbnail" style="max-width: 300px; max-height: 300px;">
                                <img src="<?php echo load_image($p->path_upload.$row->{$p->column_name}); ?>" alt="" />
                            </div>
                            <?php endif ?>
                            <?php if ($authorize != 'readonly'): ?>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 300px;"> </div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="<?php echo $p->column_name; ?>" id="<?php echo $p->column_name; ?>" accept="image/*"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                            <?php endif ?>
                        </div>
                      </div>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'int'): ?>
                      <input type="text" class="form-control input-int" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>"  value="<?php echo (isset($row)) ? $row->{$p->column_name} : $default_value; ?>" placeholder="integer"/>
                      <span class="help-block"><?php echo $p->info; ?></span>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type =='numeric'): ?>
                      <input type="text" class="form-control input-numeric" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>"  value="<?php echo (isset($row)) ? $row->{$p->column_name} : $default_value; ?>" placeholder="numeric"/>
                      <span class="help-block"><?php echo $p->info; ?></span>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type =='int_separator'): ?>
                      <input type="text" class="form-control input-int-separator" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>"  value="<?php echo (isset($row)) ? $row->{$p->column_name} : $default_value; ?>" placeholder="integer"/>
                      <span class="help-block"><?php echo $p->info; ?></span>
                    <!-- ---------------------------------------------------- -->
                    <?php elseif ($p->type == 'password'): ?>
                      <input type="password" class="form-control" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>"  value="" placeholder="type to change"/>
                      <span class="help-block"><?php echo $p->info; ?></span>
                    <?php elseif ($p->type == 'ckeditor'): ?>
                      <div name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>" class="editor"><?php echo (isset($row)) ? $row->{$p->column_name} : ''; ?></div>
                      <script>
                        $('#<?php echo $p->column_name ?>').summernote({
                          height: 300,
                          dialogsInBody: true
                        });
                      </script>
                    <!-- ---------------------------------------------------- -->
                    <?php else: ?>
                      <input type="text" class="form-control" name="<?php echo $p->column_name ?>" id="<?php echo $p->column_name ?>"  value="<?php echo (isset($row)) ? $row->{$p->column_name} : $default_value; ?>" placeholder="text"/>
                      <span class="help-block"><?php echo $p->info; ?></span>
                    <?php endif ?>
                </div>
              </div>
              <?php endforeach ?>

            </div>
            <input type="hidden" name="id" value="<?php echo $id; ?>" />
            <input type="hidden" name="form_id" value="<?php echo $form_id; ?>" />
            <?php if ($this->input->post('parent_id')): ?>
            <input type="hidden" name="parent_id" value="<?php echo $this->input->post('parent_id'); ?>" />
            <input type="hidden" name="parent_column" value="<?php echo $this->input->post('parent_column'); ?>" />
            <?php endif ?>
            <?php if (isset($flow_id)): ?>
              <input type="hidden" name="flow_id" value="<?php echo $flow_id; ?>" />
              <input type="hidden" name="t_flow_id" value="<?php echo $t_flow_id; ?>" class="t_flow_id"/>
            <?php endif ?>
            <?php if (!empty($this->input->post('col'))): ?>
              <?php foreach ($col = $this->input->post('col') as $key => $value): ?>
                <input type="hidden" name="col[<?php echo $key ?>]" value="<?php echo $value; ?>" />
              <?php endforeach ?>
            <?php endif ?>
          </div>
          <div class='form-actions'>
            <div class='row'>
              <div class='col-md-offset-5 col-md-7'>
                <!-- <a href="<?php echo site_url('formx/formx/table/'.$form_id) ?>" class="btn default">Kembali</a> -->
                <button type='submit' class='btn green' name='mode' value='new' >Simpan</button>
              </div>
            </div>
          </div>
      </form>
    </div>

    <?php if (!empty($id)): ?>
    <div class="form">
      <div class="">
        <?php if (!empty($form_many)): ?>
        <?php foreach ($form_many as $form_many_set): ?>
          <?php
            $p_form_id =  $form_many_set->id;
            $p_form_column = $form_many_set->parent_form_foreign_key;
          ?>
          <div id="form_table_<?php echo $p_form_id?>"></div>
          <script>
            $('#form_table_<?php echo $p_form_id?>').load('<?php echo site_url("formx/formx/table/".$p_form_id) ?>',
            {
            parent_id: "<?php echo $id ?>",
            parent_column: "<?php echo $p_form_column ?>",
            parent_form_id: "<?php echo $form_id ?>",
            authorize: "<?php echo $authorize ?>",
            <?php if(isset($flow_id)) : ?>
            flow_id: "<?php echo $flow_id ?>",
            <?php endif ?>
            view_only:"true"
            } ,
              function(){
              /* Stuff to do after the page is loaded */
            });

          </script>
        <?php endforeach ?>
        <?php endif ?>
      </div>
    </div>
    <?php endif ?>
  </div>
</div><!-- /.box -->
<script type="text/javascript">
  $('#input_form_<?php echo $form_id; ?>').submit(function(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        main.submitAjaxModal($(this));
        return false;
  });
</script>
<?php if (!empty($form->js_addon)): ?>
<script src="<?php echo base_url('assets/js/formx/'.$form->js_addon)?>" type="text/javascript"></script>
<?php endif ?>
