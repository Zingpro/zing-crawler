<div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <!-- <img src="../assets/layouts/layout4/img/logo-light.png" alt="logo" class="logo-default">  -->
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->

                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
							<form class="search-form" action="#" method="GET">
								<div class="input-group">
									<input type="text" class="form-control input-sm" placeholder="Search..." name="query_search">
									<span class="input-group-btn">
									<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
									</span>
								</div>
							</form>
							
                            <!-- BEGIN QUICK SIDEBAR TOGGLERUSER PROFILE -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> <?php echo @$user->full_name ?> </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="" class="img" src="<?php //echo load_thumb('uploads/foto_profile/'.@$user->id.'/'.@$user->foto) ?>" /> </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a class="openmodal" href="<?php echo site_url('auth/profile/change_foto') ?>">
                                        <i class="fa fa-camera-retro"></i> Change foto </a>
                                    </li>
                                    <li>
                                        <a class="openmodal" href="<?php echo site_url('auth/profile/change_password') ?>">
                                        <i class="fa fa-key"></i> Change password </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="<?php echo site_url('auth/logout') ?>">
                                        <i class="fa fa-sign-out"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER PROFILE -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
