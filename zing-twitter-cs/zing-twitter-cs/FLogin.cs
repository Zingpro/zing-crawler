﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Security.Cryptography;

namespace WindowsFormsApplication1
{
    public partial class FLogin : Form
    {
        String connectionString = @"Data Source = localhost; port = 3306; Initial Catalog = zing_crawler; User Id = root; password = '' ";

        MySqlConnection conn = new MySqlConnection(@"Data Source = localhost; port = 3306; Initial Catalog = zing_crawler; User Id = root; password = '' ");
        int global_i;

        public FLogin()
        {
            InitializeComponent();
        }

        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            conn.Open();
            global_i = 0;

            string source = txt_pass.Text;
            string hash;
            using (MD5 md5Hash = MD5.Create())
            {
                hash = GetMd5Hash(md5Hash, source);
            }

            MySqlCommand mc = conn.CreateCommand();
            mc.CommandType = CommandType.Text;
            mc.CommandText = "SELECT * FROM s_user WHERE USER_NAME='" + txt_user.Text + "' AND USER_PASSWORD='" + hash + "'";
            mc.ExecuteNonQuery();
            
            MessageBox.Show(hash, "Sukses");

            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(mc);
            da.Fill(dt);
            global_i = Convert.ToInt32(dt.Rows.Count.ToString());

            if (global_i > 0)
            {
                MessageBox.Show("Login Sukses","Sukses");
                this.Hide();
                FTwitter form = new FTwitter();
                form.MdiParent = this;
                form.Show();
            }
            else
            {
                MessageBox.Show("Periksa Kembali User dan Password Anda", "Sukses");
            }
            conn.Close();
        }
    }
}
