﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FMain : Form
    {
        public FMain()
        {
            InitializeComponent();
        }

        private void configToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FLogin flogin = new FLogin();
            flogin.MdiParent = this;
            flogin.Show();
        }

        private void formTwitterCrawlerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FTwitter form = new FTwitter();
            form.MdiParent = this;
            form.Show();
        }

        private void userManagemenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FUser form = new FUser();
            form.MdiParent = this;
            form.Show();
        }
    }
}
