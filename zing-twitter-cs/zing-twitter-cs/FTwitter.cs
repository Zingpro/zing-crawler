﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Windows;
using System.Xml;
using System.Collections;
using HtmlAgilityPack;
using MySql.Data.MySqlClient;
using System.Threading;
using System.Timers;
using System.Runtime.InteropServices;

namespace WindowsFormsApplication1
{
    public partial class FTwitter : Form
    {
        private const String DBServer = "localhost";
        private const String DBName = "zing_crawler";
        private const String DBUser = "root";
        private const String DBPassword = "";

        public static System.Timers.Timer theTimer;
        public static Boolean startCrawler = false;

        public static String connectionString = @"Server=" + DBServer + ";Database=" + DBName + ";Uid=" + DBUser + ";Pwd=" + DBPassword + ";";

        String URL_AKUN_TWITTER = "https://twitter.com/rtmcjatim?lang=en";

        public FTwitter()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
                       
        }

        public void browse_image(PictureBox pc, Label lbl)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "JPG|*.jpg| JPEG|*.jpeg| PNG|*.png| GIF|*.gif";
            ofd.Title = "Select image File";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label2.Text = ofd.FileName;
                pc.ImageLocation = ofd.FileName;
            }
            if (lbl.Text != "")
            {
                byte[] fimage = File.ReadAllBytes(pc.ImageLocation);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        public static string Get_HTML(string Url)
        {
            System.Net.WebResponse Result = null;
            string Page_Source_Code;
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(Url);
                Result = req.GetResponse();
                System.IO.Stream RStream = Result.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(RStream);
                new System.IO.StreamReader(RStream);

                Page_Source_Code = sr.ReadToEnd();
                sr.Dispose();
            }
            catch
            {
                // error while reading the url: the url dosen’t exist, connection problem...
                Page_Source_Code = "";
            }
            finally
            {
                if (Result != null) Result.Close();
            }
            return Page_Source_Code;
        }

        public static string Get_HTML2(string Url)
        {
            WebClient client = new WebClient();

            // Add a user agent header in case the 
            // requested URI contains a query.

            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

            Stream data = client.OpenRead(Url);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            //Console.WriteLine(s);
            data.Close();
            reader.Close();
            client.Dispose();
            return s;
        }


        public static string getBetweenString(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        public void show_html(string data)
        {
            webBrowser1.Navigate("about:blank");
            while (webBrowser1.Document == null || webBrowser1.Document.Body == null)
                Application.DoEvents();
            webBrowser1.Document.OpenNew(true).Write(data);
        }

        public static void save_data_grab_twitter(ArrayList id, ArrayList post, ArrayList akun, string keyword)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                try
                {
                    string ck = "";
                    for (int i = 0; i < id.Count; i++)
                    {
                        mysqlCon.Open();
                        ck = id[i].ToString();
                        Console.WriteLine("insert : " + ck);
                        String sqltext = "INSERT INTO twitter (twitter_id,twitter_post,twitter_profile,twitter_keyword) VALUES ('"
                            + id[i].ToString().Trim() +
                            "','" + post[i].ToString().Trim() +
                            "','" + akun[i].ToString().Trim() +
                            "','" + keyword.Trim() + "')";
                        MySqlCommand cmd = new MySqlCommand(sqltext, mysqlCon);
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                        mysqlCon.Close();
                    }
                }catch(MySqlException ex)
                {
                    Console.WriteLine("Error : "+ex.Number);
                }                
            }
        }

        public static void search_and_grap_keyword(string keyword)
        {
            if (keyword == "")
            {
                Console.WriteLine("Keyword belum diisi");                
            }
            else
            {
                try
                {
                    string keyword_real = keyword;
                    keyword = keyword.Replace(" ", "%20");

                    //string s_url = "https://twitter.com/search?f=tweets&q=" + keyword + "&src=typd";
                    string s_url = "https://twitter.com/search?f=tweets&vertical=default&q=" + keyword + "&l=id&src=typd";

                    var html = @s_url;
                    HtmlWeb web = new HtmlWeb();
                    var htmlDoc = web.Load(html);

                    // var htmlNodes = htmlDoc.DocumentNode.SelectNodes("//div/table");
                    var htmlNodes = htmlDoc.DocumentNode.SelectNodes("//table[contains(@class, 'tweet')]");
                    var htmlNodesUser = htmlDoc.DocumentNode.SelectNodes("//table[contains(@class, 'tweet')]/tr[contains(@class,'tweet-header')]/td[contains(@class,'user-info')]");
                    var htmlTweet_id = htmlDoc.DocumentNode.SelectNodes("//div[contains(@class, 'tweet-text')]");

                    String comulate_grap = "";

                    ArrayList grab_node = new ArrayList();
                    foreach (var node in htmlNodes)
                    {
                        grab_node.Add(node.OuterHtml);
                        comulate_grap += node.OuterHtml;
                    }

                    ArrayList grab_user = new ArrayList();
                    foreach (var node in htmlNodesUser)
                    {
                        //Console.WriteLine(node.OuterHtml);
                        grab_user.Add(node.OuterHtml);
                    }

                    ArrayList grab_tweet_text_id = new ArrayList();
                    foreach (var node in htmlTweet_id)
                    {
                        //Console.WriteLine(node.GetAttributeValue("data-id", "default"));
                        grab_tweet_text_id.Add(node.GetAttributeValue("data-id", "default"));
                    }

                    //grab_node = "<table>" + grab_node + "</table>";
                    //show_html(comulate_grap);

                    save_data_grab_twitter(grab_tweet_text_id, grab_node, grab_user, keyword_real);
                }
                catch (WebException ex)
                {
                    if (ex.Status == WebExceptionStatus.ProtocolError)
                    {
                        var response = ex.Response as HttpWebResponse;
                        if (response != null)
                        {
                            Console.WriteLine("HTTP Status Code: " + (int)response.StatusCode);
                        }
                        else
                        {
                            // no http status code available
                            Console.WriteLine("Error Selain HTTP : " + ex.Status.ToString());
                        }
                    }
                    else
                    {
                        // no http status code available
                        Console.WriteLine("Error Selain HTTP : " +ex.Status.ToString());
                    }
                }
            }
        }

        public static void populate_twitter()
        {
            MySqlDataAdapter adapter;
            DataTable tbl = new DataTable();
            
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                try
                {
                    Console.WriteLine("Select Keyword From DB keyword");
                    String sqltext = "SELECT * FROM keyword";

                    adapter = new MySqlDataAdapter(sqltext, mysqlCon);
                    adapter.Fill(tbl);

                    foreach (DataRow row in tbl.Rows)
                    {
                        string jg = row["keyword_text"].ToString();
                        Console.WriteLine("Getting Keyword : " + jg);

                        Action onCompleted = () =>
                        {
                            //On complete action
                            Console.WriteLine("Finish Grap : " + jg);
                            ShowAutoClosingMessageBox("Grap " + jg + " Terlaksana", jg);
                        };

                        Thread th = new Thread(t =>
                        {
                            try
                            {
                                search_and_grap_keyword2(jg);
                            }
                            finally
                            {
                                onCompleted();
                            }
                        })
                        { IsBackground = true };
                        th.Start();
                    }
                    mysqlCon.Close();

                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error : " + ex.Number);
                }
            }            
        }

        public static void search_and_grap_keyword2(string keyword)
        {
            if (keyword == "")
            {
                Console.WriteLine("Keyword belum diisi");
            }
            else
            {
                try
                {
                    string keyword_real = keyword;
                    keyword = keyword.Replace(" ", "%20");

                    //string s_url = "https://twitter.com/search?f=tweets&q=" + keyword + "&src=typd";
                    string s_url = "https://twitter.com/search?f=tweets&vertical=default&q=" + keyword + "&l=id&src=typd";
                                        
                    string html = string.Empty;

                    int tries = 5;
                    while (tries > 0)
                    {
                        using (var client = new WebClient())
                        {
                            client.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0");
                            try
                            {
                                html = client.DownloadString(s_url);
                                tries--;
                                if (!string.IsNullOrEmpty(html))
                                {
                                    break;
                                }
                            }
                            catch (WebException ex)
                            {
                                tries--;
                                Console.WriteLine(s_url);
                                Console.WriteLine("Error nih " + ex.Message);
                            }
                        }
                    }

                    //Console.WriteLine(html);
                    var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                    htmlDoc.LoadHtml(html);

                    var htmlNodes = htmlDoc.DocumentNode.SelectNodes("//table[contains(@class, 'tweet')]");
                    var htmlNodesUser = htmlDoc.DocumentNode.SelectNodes("//table[contains(@class, 'tweet')]/tr[contains(@class,'tweet-header')]/td[contains(@class,'user-info')]");
                    var htmlTweet_id = htmlDoc.DocumentNode.SelectNodes("//div[contains(@class, 'tweet-text')]");

                    String comulate_grap = "";

                    ArrayList grab_node = new ArrayList();
                    foreach (var node in htmlNodes)
                    {
                        grab_node.Add(node.OuterHtml);
                        comulate_grap += node.OuterHtml;
                    }

                    ArrayList grab_user = new ArrayList();
                    foreach (var node in htmlNodesUser)
                    {
                        //Console.WriteLine(node.OuterHtml);
                        grab_user.Add(node.OuterHtml);
                    }

                    ArrayList grab_tweet_text_id = new ArrayList();
                    foreach (var node in htmlTweet_id)
                    {
                        //Console.WriteLine(node.GetAttributeValue("data-id", "default"));
                        grab_tweet_text_id.Add(node.GetAttributeValue("data-id", "default"));
                    }

                    //grab_node = "<table>" + grab_node + "</table>";
                    //show_html(comulate_grap);

                    save_data_grab_twitter(grab_tweet_text_id, grab_node, grab_user, keyword_real);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string kata = textBox1.Text;
            if(kata == "")
            {
                populate_twitter();
            }else
            {
                search_and_grap_keyword(kata);
            }                       
        }

        private void FTwitter_Load(object sender, EventArgs e)
        {

        }
            

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(checkBox1.Checked))
            {
                startCrawler = true;
                int waktu = Int32.Parse(textBox2.Text);
                theTimer = new System.Timers.Timer(waktu);
                // Hook up the Elapsed event for the timer.
                theTimer.Elapsed += OnTimedEvent;
                theTimer.Enabled = true;
            }
            else
            {
                theTimer.Enabled = false;
                startCrawler = false;
            }
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (startCrawler == true)
            {
                populate_twitter();
                ShowAutoClosingMessageBox("crawler dilaksanakan : {"+ e.SignalTime.ToString() + "}","Pelaksanaan Crawler");
            }
        }

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.Dll")]
        static extern int PostMessage(IntPtr hWnd, UInt32 msg, int wParam, int lParam);

        private const UInt32 WM_CLOSE = 0x0010;

        public static void ShowAutoClosingMessageBox(string message, string caption)
        {
            var timer = new System.Timers.Timer(5000) { AutoReset = false };
            timer.Elapsed += delegate
            {
                IntPtr hWnd = FindWindowByCaption(IntPtr.Zero, caption);
                if (hWnd.ToInt32() != 0) PostMessage(hWnd, WM_CLOSE, 0, 0);
            };
            timer.Enabled = true;
            MessageBox.Show(message, caption);
        }
    }
}
