﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApplication1
{
    public partial class FUser : Form
    {
        private const String DBServer = "localhost";
        private const String DBName = "zing_crawler";
        private const String DBUser = "root";
        private const String DBPassword = "";

        String connectionString = @"Server=" + DBServer + ";Database=" + DBName + ";Uid=" + DBUser + ";Pwd=" + DBPassword + ";";
        public FUser()
        {
            InitializeComponent();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        void gridfill(DataGridView dgv)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();

                //MySqlDataAdapter sqla = new MySqlDataAdapter("view_twitter",mysqlCon);
                //sqla.SelectCommand.CommandType = CommandType.StoredProcedure;
                MySqlDataAdapter sqla = new MySqlDataAdapter("SELECT * FROM s_user", mysqlCon);               
                sqla.SelectCommand.CommandType = CommandType.Text;
                //sqla.SelectCommand.CommandText = "";

                DataTable tbl1 = new DataTable();
                sqla.Fill(tbl1);
                dgv.DataSource = tbl1;
            }
        }

        void insert_twitter(String id, String post)
        {
            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                mysqlCon.Open();
                MySqlCommand cmd = new MySqlCommand("add_twitter", mysqlCon);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("_twitter_id", id.Trim());
                cmd.Parameters.AddWithValue("_twitter_post", post.Trim());
                cmd.ExecuteNonQuery();
                MessageBox.Show("Submitted Successfully");
            }
        }
        private void FUser_Load(object sender, EventArgs e)
        {
            gridfill(dataGridView1);
        }
    }
}
