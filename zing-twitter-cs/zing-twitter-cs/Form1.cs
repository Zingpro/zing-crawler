﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Windows;
using System.Xml;
using System.Collections;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
                       
        }

        public void browse_image(PictureBox pc, Label lbl)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "JPG|*.jpg| JPEG|*.jpeg| PNG|*.png| GIF|*.gif";
            ofd.Title = "Select image File";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label2.Text = ofd.FileName;
                pc.ImageLocation = ofd.FileName;
            }
            if (lbl.Text != "")
            {
                byte[] fimage = File.ReadAllBytes(pc.ImageLocation);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        public static string Get_HTML(string Url)
        {
            System.Net.WebResponse Result = null;
            string Page_Source_Code;
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(Url);
                Result = req.GetResponse();
                System.IO.Stream RStream = Result.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(RStream);
                new System.IO.StreamReader(RStream);

                Page_Source_Code = sr.ReadToEnd();
                sr.Dispose();
            }
            catch
            {
                // error while reading the url: the url dosen’t exist, connection problem...
                Page_Source_Code = "";
            }
            finally
            {
                if (Result != null) Result.Close();
            }
            return Page_Source_Code;
        }

        public static string Get_HTML2(string Url)
        {
            WebClient client = new WebClient();

            // Add a user agent header in case the 
            // requested URI contains a query.

            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

            Stream data = client.OpenRead(Url);
            StreamReader reader = new StreamReader(data);
            string s = reader.ReadToEnd();
            Console.WriteLine(s);
            data.Close();
            reader.Close();
            client.Dispose();
            return s;
        }

        static IEnumerable<HtmlElement> ElementsByClass(HtmlDocument doc, string className)
        {
            foreach (HtmlElement e in doc.All)
                if (e.GetAttribute("className") == className)
                    yield return e;
        }

        public static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {            
            string data;
            string keyword;
            label2.Text = "Status : On Loading...";
            keyword = textBox1.Text;
            if (keyword == "")
            {                
                MessageBox.Show("Keyword belum diisi","Error");
                label2.Text = "Status : idle";
            }
            else
            {
                keyword = keyword.Replace(" ", "%20");
                //data = Get_HTML(textBox1.Text);
                //richTextBox1.Text = data;
                //string s_url = "https://twitter.com/search?f=tweets&q=" + keyword + "&src=typd";
                string s_url = "https://twitter.com/search?f=tweets&vertical=default&q=" + keyword + "&l=id&src=typd";
                //webBrowser1.Navigate(s_url);
                
                data = Get_HTML2(s_url);
                label2.Text = "Status : Data Received.";
                richTextBox2.Text = data;
                //string[] list_data = data.Split(new string[] { "<tr class=\"tweet-container\">" }, StringSplitOptions.None);
                data = getBetween(data, "<body class=\"images nojs searches-page searches-show-page\">", "</body>");
                
                webBrowser1.Navigate("about:blank");
                while (webBrowser1.Document == null || webBrowser1.Document.Body == null)
                    Application.DoEvents();
                webBrowser1.Document.OpenNew(true).Write(data);
                /*
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(data);

                XmlNode root = xmlDoc.DocumentElement;
                IEnumerator ienum = root.GetEnumerator();
                XmlNode book;
                while (ienum.MoveNext())
                {
                    book = (XmlNode)ienum.Current;
                    MessageBox.Show(book.OuterXml);
                }
                */

                /*
                for (int i = 1; i < list_data.Length; i++)
                {
                    list_data[i] = list_data[i].Replace("</tr>", "");
                    //list_data[i] = list_data[i].Replace("</td>", "");
                    //list_data[i] = list_data[i].Replace("</a>", "");
                    //list_data[i] = list_data[i].Replace("</span>", "");
                    //list_data[i] = list_data[i].Replace("</div>", "");

                    //list_data[i] = getBetween(list_data[i], "<div class=\"tweet-text\" data-id=\"", "\">");
                    list_data[i] = getBetween(list_data[i], "<td", "</td>");
                    richTextBox1.Text += list_data[i] + "\n";
                }*/
            }

            //HtmlDocument doc = webBrowser1.Document;
            //string datas = ElementsByClass(doc, "tweet-container").ToString();
            //MessageBox.Show(datas);
        }
    }
}
