﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using HtmlAgilityPack;
using MySql.Data.MySqlClient;

namespace zing_twitter_uloc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private const String DBServer = "localhost";
        private const String DBName = "zing_crawler";
        private const String DBUser = "root";
        private const String DBPassword = "";

        public static System.Timers.Timer theTimer;
        public static Boolean startCrawler = false;

        public static String connectionString = @"Server=" + DBServer + ";Database=" + DBName + ";Uid=" + DBUser + ";Pwd=" + DBPassword + ";";

        String URL_AKUN_TWITTER = "https://twitter.com/rtmcjatim?lang=en";


        public static void get_list_user()
        {
            MySqlDataAdapter adapter;
            DataTable tbl = new DataTable();

            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                try
                {
                    Console.WriteLine("Select User From DB twitter");

                    String sqltext = "SELECT * FROM twitter WHERE twitter_username is null";

                    adapter = new MySqlDataAdapter(sqltext, mysqlCon);
                    adapter.Fill(tbl);

                    if (tbl.Rows.Count > 0)
                    {
                        foreach (DataRow row in tbl.Rows)
                        {
                            string s_user = row["twitter_profile"].ToString();
                            string twitter_id = row["twitter_id"].ToString();

                            s_user = getBetweenString(s_user, "<span>@</span>", "</div>");

                            // Console.WriteLine("Getting Profile : " + s_user);
                            update_twitter_user(twitter_id, s_user);

                        } // end foreach
                        //Console.WriteLine(tbl.Rows.Count + " Data telah di update");
                        ShowAutoClosingMessageBox(tbl.Rows.Count + " Data telah di update", "Auto Pesan");
                    }
                    else
                    {
                        //Console.WriteLine("Data Kosong - Tidak Ada yang diupdate");
                        ShowAutoClosingMessageBox("Data Kosong - Tidak Ada yang diupdate","Auto Pesan");
                    }
                    mysqlCon.Close();
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error : " + ex.Number);
                }
            } // end using MysqlConnection
        }

        public static void update_twitter_user(string id, string user)
        {
            MySqlConnection Conn = new MySqlConnection(connectionString);
            Conn.Open();
            try
            {
                Console.WriteLine("Update ID :"+id+", User :"+user);

                String sqltext = "UPDATE twitter SET twitter_username = '"+user+"' WHERE twitter_id ='"+id+"'";

                MySqlCommand cmd = new MySqlCommand(sqltext, Conn);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                                        
                Conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error : " + ex.Number);
            }
            
        }

        public static void update_twitter_location(string id, string location)
        {
            MySqlConnection Conn = new MySqlConnection(connectionString);
            Conn.Open();
            try
            {
                Console.WriteLine("Update ID :" + id + ", Location :" + location);

                String sqltext = "UPDATE twitter SET twitter_user_location = '" + location + "' WHERE twitter_id ='" + id + "'";
                //Console.WriteLine(sqltext);
                MySqlCommand cmd = new MySqlCommand(sqltext, Conn);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();

                Conn.Close();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error : " + ex.Number);
            }

        }

        public static string getBetweenString(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }

        public static string search_and_grap_user_location(string username)
        {
            
            if (username == "")
            {
                Console.WriteLine("Keyword belum diisi");
                return "";
            }
            else
            {
                try
                {
                    string location = "";
                    username = username.Replace(" ", "");
                                        
                    string s_url = "https://twitter.com/"+ username;

                    string html = string.Empty;

                    int tries = 5;
                    while (tries > 0)
                    {
                        using (var client = new WebClient())
                        {
                            
                            client.Headers.Add(HttpRequestHeader.UserAgent, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20120101 Firefox/33.0");
                            try
                            {
                                html = client.DownloadString(s_url);
                                tries--;
                                if (!string.IsNullOrEmpty(html))
                                {
                                    break;
                                }
                            }
                            catch (WebException ex)
                            {
                                tries--;
                                Console.WriteLine(s_url);
                                Console.WriteLine("Error nih "+ex.Message);
                            }
                        }
                    }

                    //Console.WriteLine(html);
                    var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                    htmlDoc.LoadHtml(html);

                    var htmlNodes = htmlDoc.DocumentNode.SelectSingleNode("//div[contains(@class, 'location')]");
                    if (htmlNodes != null)
                    {
                        location = htmlNodes.InnerHtml;
                    }

                    return location;
                }
                catch (HtmlWebException ex)
                {   
                    Console.WriteLine(ex.Message);
                    return "";
                }
            }
        }

        public static void gen_user_location()
        {
            MySqlDataAdapter adapter;
            DataTable tbl = new DataTable();

            using (MySqlConnection mysqlCon = new MySqlConnection(connectionString))
            {
                try
                {
                    Console.WriteLine("Select User From DB twitter");

                    String sqltext = "SELECT * FROM twitter WHERE twitter_user_location is null LIMIT 20";

                    adapter = new MySqlDataAdapter(sqltext, mysqlCon);
                    adapter.Fill(tbl);

                    if (tbl.Rows.Count > 0)
                    {
                        foreach (DataRow row in tbl.Rows)
                        {
                            string twitter_username = row["twitter_username"].ToString();
                            string twitter_id = row["twitter_id"].ToString();

                            string location = search_and_grap_user_location(twitter_username);

                            Console.WriteLine("User Location : " + location);
                            update_twitter_location(twitter_id, location);

                        } // end foreach
                        //Console.WriteLine(tbl.Rows.Count + " Data telah di update");
                        ShowAutoClosingMessageBox(tbl.Rows.Count + " Data telah di update", "Auto Pesan");
                    }
                    else
                    {
                        //Console.WriteLine("Data Kosong - Tidak Ada yang diupdate");
                        ShowAutoClosingMessageBox("Data Kosong - Tidak Ada yang diupdate", "Auto Pesan");
                    }
                    mysqlCon.Close();
                }
                catch (MySqlException ex)
                {
                    Console.WriteLine("Error : " + ex.Number);
                }
            } // end using MysqlConnection
        }

        private void button1_Click(object sender, EventArgs e)
        {
            get_list_user();
        }


        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.Dll")]
        static extern int PostMessage(IntPtr hWnd, UInt32 msg, int wParam, int lParam);

        private const UInt32 WM_CLOSE = 0x0010;

        public static void ShowAutoClosingMessageBox(string message, string caption)
        {
            var timer = new System.Timers.Timer(5000) { AutoReset = false };
            timer.Elapsed += delegate
            {
                IntPtr hWnd = FindWindowByCaption(IntPtr.Zero, caption);
                if (hWnd.ToInt32() != 0) PostMessage(hWnd, WM_CLOSE, 0, 0);
            };
            timer.Enabled = true;
            MessageBox.Show(message, caption);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            gen_user_location();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (Convert.ToBoolean(checkBox1.Checked))
            {
                startCrawler = true;
                int waktu = Int32.Parse(textBox1.Text);
                theTimer = new System.Timers.Timer(waktu);
                // Hook up the Elapsed event for the timer.
                theTimer.Elapsed += OnTimedEvent;
                theTimer.Enabled = true;
            }
            else
            {
                theTimer.Enabled = false;
                startCrawler = false;
            }
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (startCrawler == true)
            {
                get_list_user();
                gen_user_location();
                Console.WriteLine("crawler dilaksanakan : {0}", e.SignalTime);
            }
        }
    }
}
